<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>HỘI NHÀ BÁO VIỆT NAM</title>
    <!--Bootstrap -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="public/js/bootstrap.js" rel="stylesheet">
    <!--Bootstrap icons-->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <style>
        div.title{
            padding: 50px;
            text-align: center;
        }

        .title-h1{
            margin-bottom: 0;
            color: darkblue;
        }

        .title-h4{
            color: darkred;
        }

        div.title-member{
            width: 90%;
            margin-left: auto;
            margin-right: auto;
            color: darkgreen;
        }

        .table-member{
            text-align: center;
            width: 90%;
            margin-left: auto;
            margin-right: auto;
        }

        td.actions{
            width: 190px;
            padding-left: 0;
            padding-right: 0;
        }

        .main{
            width: 90%;
            margin-left: auto;
            margin-right: auto;
            margin-top: 50px;
        }

        .article{
            float: left;
            width: 65%;
        }

        div.title-article{
            width: 90%;
            margin-right: auto;
            color: darkgreen;
        }

        .table-article{
            text-align: center;
            margin-right: auto;
            width: 90%;
        }

        td.author{
            width: 15%;
        }

        td.date{
            width: 20%;
        }

        .prize{
            float: right;
            width: 35%;
        }

        div.title-prize{
            margin-right: auto;
            color: darkgreen;
        }
        .table-prize{
            text-align: center;
            margin-right: auto;
        }

        td.action{
            width: 30%;
            padding-left: 0;
            padding-right: 0;
        }

        .clear{
            clear: both;
        }

        .footer{
            margin-top: 30px;
            margin-bottom: 20px;
            text-align: center;
        }

        .form-login{
            width: 50%;
            margin-left: auto;
            margin-right: auto;
        }

        .text-login{
            width: 80%;
        }

        .thin-panel {
            width: 50%;
            margin-right: auto;
            margin-left: auto;
        }
        .tt{
            text-align: center;
            color: darkred;
            font-size: 30px;
            margin-right: auto;
            margin-left: auto;
            margin-bottom: 50px;
        }
        .ct{
            margin-right: auto;
            margin-left: auto;
        }

        .inf{
            float: left;
            width: 50%;

        }

        .infor{
            width: 80%;
            margin-right: auto;
            margin-left: auto;
        }

        .list{
            float: right;
            width: 50%;
        }

        .inf-article{
            width: 90%;
            margin-left: auto;
            margin-right: auto;
        }

    </style>
</head>
<body>
<div class="container">
    <div class="title-box title">
        <h1 class="title-h1">HỘI NHÀ BÁO VIỆT NAM</h1>
        <h4 class="title-h4">VIETNAM JOURNALISTS ASSOCIATION</h4>
    </div>

</div>
</body>
</html>