<?php include_once 'header.php';?>

    <div class="thin-panel">
        <div class="d-flex justify-content-between">
            <div>
                <h3>Thông tin bài báo</h3>
            </div>
            <div>
                <a href="index.php?controller=member&action=user" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Return </a>
            </div>
        </div>
        <hr/>
        <form action="index.php?controller=article&action=updateArticle2" method="post">
            <?php while ($row = $data["article"]->fetch_assoc()){ ?>
                <input type="hidden" name="id_article" class="form-group" id="id_article" value="<?php echo $row['ID_ARTICLE']; ?>">
                <input type="hidden" name="id_user" class="form-group" id="id_user" value="<?php echo $row['ID_USER']; ?>">
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" id="title" value="<?php echo $row['TITLE']; ?>" required>
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea name="content" class="form-control" id="content" rows="10" required><?php echo $row['CONTENT']; ?></textarea>
            </div>
            <div class="form-group">
                <label for="category">Category</label>
                <input type="text" name="category" class="form-control" id="category" value="<?php echo $row['CATEGORY']; ?>" required>
            </div>
            <div class="form-group">
                <label for="create_date">Create Date</label>
                <input type="date" name="create_date" class="form-control" id="create_date" value="<?php echo $row['CREATE_DATE']; ?>" required>
            </div>
            <div class="form-group">
                <label for="prize">Prize</label>
                <br/>
                <select name="medals">
<!--                    <option value="1">Huy chương vàng</option>-->
<!--                    <option value="2">Huy chương bạc</option>-->
                    <option value="3">Huy chương đồng</option>
                </select>
            </div>
            <br/>

            <button type="submit" class="btn btn-primary"> EDIT </button>
            <?php } ?>
        </form>
    </div>


<?php include_once 'footer.php';?>