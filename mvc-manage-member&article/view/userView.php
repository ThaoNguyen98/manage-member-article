<?php include_once 'header.php';

?>
<div class="conteiner">
    <div class="thin-panel inf">
        <div class="d-flex justify-content-between infor">
            <div>
                <h3>Thông tin cá nhân</h3>
            </div>
            <div>
                <a href="index.php?controller=member&action=nomal" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Return </a>
            </div>
        </div>
        <br/>
        <form action="index.php?controller=member&action=updateUser" class="infor" method="post" >

            <?php while ($row = $data["member"]->fetch_assoc()){ ?>

                <input type="hidden" name="id_user" value="16">
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" name="first_name" class="form-control" id="first_name" value="<?php echo $row['FIRST_NAME']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="surname">Surname</label>
                    <input type="text" name="surname" class="form-control" id="surname" value="<?php echo $row['SURNAME']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" name="username" class="form-control" id="username" value="<?php echo $row['USERNAME']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" value="<?php echo $row['PASSWORD']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="join_date">Join Date</label>
                    <input type="date" name="join_date" class="form-control" id="join_date" value="<?php echo $row['JOIN_DATE']; ?>" required>
                </div>
                <div class="form-group">
                    <label for="hometown">Hometown</label>
                    <input type="text" name="hometown" class="form-control" id="hometown" value="<?php echo $row['HOMETOWN']; ?>" required>
                </div>
                <div class="form-group">
                    <label>Role</label>
                    <br/>
                    <select name="role">
<!--                        <option --><?php //if($row['ROLE'] == 1) echo "selected"; ?><!--  value="1">Manage</option>-->
                        <option <?php if($row['ROLE'] == 0) echo ""; ?>  value="0">Normal</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary" name="submit"> EDIT </button>
            <?php } ?>
        </form>
    </div>
    <div class="list">
        <div class="d-flex justify-content-between inf-article">
            <div>
                <h3>Danh sách các bài báo</h3>
            </div>
            <div>

                <a class="btn btn-outline-primary" href="index.php?controller=article&action=newArticle2&id_user=<?php echo $_SESSION['id'] ; ?>"><i class="fa fa-sign-in" aria-hidden="true"></i> ARTICLE</a>

            </div>
        </div>
        <br/>
        <br/>
        <table class="table table-bordered grocery-crud-table table-hover inf-article">
            <tr>
                <th>TITLE</th>
                <th>CREATE DATE</th>
                <th>ACTIONS</th>
            </tr>
            <?php foreach ($data["article"] as $article) { ?>
                <tr>
                    <td>
                        <a href="index.php?controller=article&action=article2View&id_article=<?php echo $article['ID_ARTICLE']; ?>"><?php echo $article['TITLE']; ?></a>
                    </td>
                    <td class="date"><?php echo $article['CREATE_DATE']; ?></td>
                    <td class="actions">
                        <a class="btn btn-outline-success" href="index.php?controller=article&action=deleteArticle2&id_article=<?php echo $article['ID_ARTICLE'] ; ?>" ><i class="fa fa-trash-o" aria-hidden="true"></i> DELETE </a>
                        <a class="btn btn-outline-danger" href="index.php?controller=article&action=editArticle2&id_article=<?php echo $article['ID_ARTICLE'] ; ?>" ><i class="fa fa-edit" aria-hidden="true"></i> EDIT </a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>

    <div class="clear">

    </div>
</div>

<?php include_once 'footer.php';?>
