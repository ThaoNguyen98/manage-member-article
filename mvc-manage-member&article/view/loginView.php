<?php include_once 'header.php';?>

<body>
<div class="container">
    <form action="index.php?controller=member&action=login" class="form-login" method="post">

        <div class="form-group">
            <label>USERNAME</label>
            <input type="text" name="username" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="password">PASSWORD</label>
            <input type="password" name="password" class="form-control" id="password" placeholder="123" required>
        </div>
        <div>
            <?php
                if (isset($message))
                    echo $message;
            ?>
        </div>
        <br/>
        <button type="submit" class="btn btn-primary" name="submit" > LOG IN </button>

        <br/>
        <br/>
        <p>Chưa có tài khoản? Đăng ký ngay! <a class="btn btn-success" href="index.php?controller=member&action=register"> REGISTER </a></p>

    </form>

</div>
</body>

<?php include_once 'footer.php';?>
