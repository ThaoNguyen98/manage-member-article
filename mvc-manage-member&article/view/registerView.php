<?php include_once 'header.php';?>

<div class="thin-panel">
    <div class="d-flex justify-content-between">
        <div>
            <h3>Đăng ký</h3>
        </div>
        <div>
            <a href="index.php?controller=member&action=login" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Return </a>
        </div>
    </div>
    <hr/>
    <form action="index.php?controller=member&action=registerAc" method="post">
        <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Văn Bác" required>
        </div>
        <div class="form-group">
            <label for="surname">Surname</label>
            <input type="text" name="surname" class="form-control" id="surname" placeholder="Nguyễn" required>
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" class="form-control" id="username" placeholder="ole-ola" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control" id="password" placeholder="123" required>
        </div>
        <div class="form-group">
            <label for="join_date">Join Date</label>
            <input type="date" name="join_date" class="form-control" id="join_date" placeholder="dd-mm-yyyy" required>
        </div>
        <div class="form-group">
            <label for="hometown">Hometown</label>
            <input type="text" name="hometown" class="form-control" id="hometown" placeholder="Hà Nội" required>
        </div>
        <div class="form-group">
            <label>Role</label>
            <br/>
            <select name="role">
<!--                <option value="1">Manage</option>-->
                <option value="0">Normal</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary" name="submit"> ADD </button>
    </form>
</div>

<?php include_once 'footer.php';?>
