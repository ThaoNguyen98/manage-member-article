<?php
/**
 * Created by PhpStorm.
 * User: DaiPhongPC
 * Date: 7/27/2018
 * Time: 9:55 AM
 */
include_once 'header.php';
?>

<div class="thin-panel">
    <div class="d-flex justify-content-between">
        <div>
            <h3>Giải thưởng</h3>
        </div>
        <div>
            <a href="index.php?controller=member&action=index" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Return </a>
        </div>
    </div>
    <hr/>
    <form action="index.php?controller=prize&action=updatePrize" method="post">
        <?php while ($row = $data["prize"]->fetch_assoc()) { ?>
            <input type="hidden" name="id_prize" class="form-group" value="<?php echo $row['ID_PRIZE']; ?>">

            <div class="form-group">
                <label>PRIZE ID : </label>
                <span><?php echo $row['ID_PRIZE'] ; ?></span>
                <!--            <input type="text" name="id_prize" class="form-control" value="--><?php //echo $row['ID_PRIZE']; ?><!--">-->
            </div>
            <div class="form-group">
                <label>ARTICLE ID : </label>
                <span><?php echo $row['ID_ARTICLE']; ?></span>
                <!--            <input type="text" name="id_article" class="form-control" value="--><?php //echo $row['ID_ARTICLE']; ?><!--">-->
            </div>
            <div class="form-group">
                <label>Giải thưởng : </label>
                <select name="medals">
                    <option <?php if($row['MEDALS'] == 1) echo "selected"; ?> value="1">Huy chương vàng</option>
                    <option <?php if($row['MEDALS'] == 2) echo "selected"; ?> value="2">Huy chương bạc</option>
                    <option <?php if($row['MEDALS'] == 3) echo "selected"; ?> value="3">Huy chương đồng</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary"> ADD </button>
        <?php } ?>
    </form>
</div>

<?php include_once 'footer.php'; ?>
