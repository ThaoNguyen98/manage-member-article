<?php
/**
 * Created by PhpStorm.
 * User: DaiPhongPC
 * Date: 7/16/2018
 * Time: 11:07 AM
 */
include_once 'header.php';

?>

<body>
<div class="conteiner">
    <div class="title-member">
        <h3>Danh sách hội viên Hội nhà báo Việt Nam</h3>
        <div class="d-flex justify-content-between">
            <div>
                <a class="btn btn-outline-primary" href="index.php?controller=member&action=newMember"><i class="fa fa-sign-in" aria-hidden="true"></i> ADD </a>
            </div>
            <div>
                <a href="index.php?controller=member&action=login" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Return </a>
            </div>
        </div>
    </div>
    <table class="table table-bordered grocery-crud-table table-hover table-member">
        <thead>
        <tr>
            <th>FIRST NAME</th>
            <th>SURNAME</th>
            <th>USERNAME</th>
            <th>JOIN DATE</th>
            <th>HOMETOWN</th>
            <th>ROLE</th>
            <th>ACTIONS</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data["member"] as $member){ ?>
            <tr>
                <td><?php echo $member["FIRST_NAME"]; ?></td>
                <td><?php echo $member["SURNAME"]; ?></td>
                <td><?php echo $member["USERNAME"]; ?></td>
                <td><?php echo $member["JOIN_DATE"]; ?></td>
                <td><?php echo $member["HOMETOWN"]; ?></td>
                <td><?php echo $member["ROLE"]; ?></td>
                <td class="actions">
                    <a class="btn btn-outline-primary" href="index.php?controller=article&action=newArticle&id_user=<?php echo $member['ID_USER'] ; ?>"><i class="fa fa-sign-in" aria-hidden="true"></i> ARTICLE</a>
                    <a class="btn btn-outline-success" href="index.php?controller=member&action=deleteMember&id_user=<?php echo $member['ID_USER'] ; ?>" ><i class="fa fa-trash-o" aria-hidden="true"></i> DELETE</a>
                    <a class="btn btn-outline-danger" href="index.php?controller=member&action=editMember&id_user=<?php echo $member['ID_USER'] ; ?>" ><i class="fa fa-edit" aria-hidden="true"></i> EDIT</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <div class="main">
        <div class="article">
            <div class="title-article">
                <h3>Danh sách các bài báo</h3>
            </div>
            <table class="table table-bordered grocery-crud-table table-hover table-article">
                <tr>
                    <th>AUTHOR</th>
                    <th>TITLE</th>
                    <th>CREATE DATE</th>
                    <th>ACTIONS</th>
                </tr>
                <?php foreach ($data["article"] as $article){  ?>
                <tr>
                    <td class="author"><?php echo $article['ID_USER']; ?></td>
                    <td><a href="index.php?controller=article&action=articleView&id_article=<?php echo $article['ID_ARTICLE']; ?>"><?php echo $article['TITLE']; ?></a></td>
                    <td class="date"><?php echo $article['CREATE_DATE']; ?></td>
                    <td class="actions">
                        <a class="btn btn-outline-success" href="index.php?controller=article&action=deleteArticle&id_article=<?php echo $article['ID_ARTICLE'] ; ?>" ><i class="fa fa-trash-o" aria-hidden="true"></i> DELETE </a>
                        <a class="btn btn-outline-danger" href="index.php?controller=article&action=editArticle&id_article=<?php echo $article['ID_ARTICLE'] ; ?>" ><i class="fa fa-edit" aria-hidden="true"></i> EDIT </a>
                    </td>
                </tr>
                <?php } ?>
            </table>
        </div>

        <div class="prize">
            <div class="title-prize">
                <h3>Giải thưởng cho bài báo xuất sắc</h3>

            </div>
            <table class="table table-bordered grocery-crud-table table-hover table-prize">
                <thead>
                <tr>
                    <th>ARTICLE ID</th>
                    <th>MEDALS <img src="./img/gole.png" height="10%"></th>
                    <th>ACTIONS</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($data["prize"] as $prize){ ?>
                <tr>
                            <td><?php echo $prize['ID_ARTICLE']; ?></td>
                            <td><?php echo $prize['MEDALS']; ?></td>
                            <td class="action"><a class="btn btn-outline-primary" href="index.php?controller=prize&action=editPrize&id_prize=<?php echo $prize['ID_PRIZE'] ; ?>"><i class="fa fa-sign-in" aria-hidden="true"></i> UPDATE</a></td>
                </tr>
                <?php } ?>



                </tbody>
            </table>
        </div>
        <div class="clear">

        </div>
    </div>
</div>

<?php include_once 'footer.php'; ?>
