<?php include_once 'header.php';?>

    <div class="conteiner thin-panel">
        <div class="d-flex justify-content-between">
            <div>
                <h4>Bài báo</h4>
            </div>
            <div>
                <a href="index.php?controller=member&action=user" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Return </a>
            </div>
        </div>
        <hr/>
        <?php foreach ($data["article"] as $article){ ?>
            <div class="tt">
                <p><?php echo $article["TITLE"]; ?></p>
            </div>
            <div class="ct">
                <p><?php echo $article["CONTENT"]; ?></p>
            </div>
        <?php } ?>
    </div>


<?php include_once 'footer.php';?>