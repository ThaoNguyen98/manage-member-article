<?php include_once 'header.php';

?>

<div class="thin-panel">
    <div class="d-flex justify-content-between">
        <div>
            <h3>Thông tin hội viên</h3>
        </div>
        <div>
            <a href="index.php?controller=member&action=index" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Return </a>
        </div>
    </div>
    <hr/>
    <form action="index.php?controller=member&action=updateMember" method="post">

        <?php while ($row = $data["member"]->fetch_assoc()){ ?>

        <input type="hidden" name="id_user" value="<?php echo $row['ID_USER']; ?>">
        <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text" name="first_name" class="form-control" id="first_name" value="<?php echo $row['FIRST_NAME']; ?>" required>
        </div>
        <div class="form-group">
            <label for="surname">Surname</label>
            <input type="text" name="surname" class="form-control" id="surname" value="<?php echo $row['SURNAME']; ?>" required>
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" class="form-control" id="username" value="<?php echo $row['USERNAME']; ?>" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control" id="password" value="<?php echo $row['PASSWORD']; ?>" required>
        </div>
        <div class="form-group">
            <label for="join_date">Join Date</label>
            <input type="date" name="join_date" class="form-control" id="join_date" value="<?php echo $row['JOIN_DATE']; ?>" required>
        </div>
        <div class="form-group">
            <label for="hometown">Hometown</label>
            <input type="text" name="hometown" class="form-control" id="hometown" value="<?php echo $row['HOMETOWN']; ?>" required>
        </div>
        <div class="form-group">
            <label>Role</label>
            <br/>
            <select name="role">
                <option <?php if($row['ROLE'] == 1) echo "selected"; ?>  value="1">Manage</option>
                <option <?php if($row['ROLE'] == 0) echo "selected"; ?>  value="0">Normal</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary" name="submit"> EDIT </button>
        <?php } ?>
    </form>
</div>

<?php include_once 'footer.php';?>
