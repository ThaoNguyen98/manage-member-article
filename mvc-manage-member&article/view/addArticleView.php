<?php include_once 'header.php';?>

<div class="thin-panel">
    <div class="d-flex justify-content-between">
        <div>
            <h3>Thêm bài báo</h3>
        </div>
        <div>
            <a href="index.php?controller=member&action=index" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Return </a>
        </div>
    </div>
    <hr/>
    <form action="index.php?controller=article&action=insertArticle" method="post">
        <input type="hidden" name="id_user" value="<?php echo $_GET['id_user']; ?>">

        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" name="title" class="form-control" id="title" placeholder="Tiêu đề bài báo" required>
        </div>
        <div class="form-group">
            <label for="content">Content</label>
            <textarea name="content" class="form-control" id="content" rows="10" placeholder="Nội dung bài báo" required></textarea>
        </div>
        <div class="form-group">
            <label for="category">Category</label>
            <input type="text" name="category" class="form-control" id="category" placeholder="Giải trí" required>
        </div>
        <div class="form-group">
            <label for="create_date">Create Date</label>
            <input type="date" name="create_date" class="form-control" id="create_date" placeholder="dd/mm/yyyy" required>
        </div>

        <div class="form-group">
            <label>Prize</label>
            <br/>
            <select name="medals">
                <option value="1">Huy chương vàng</option>
                <option value="2">Huy chương bạc</option>
                <option value="3">Huy chương đồng</option>
            </select>
        </div>
        <br/>

        <button type="submit" class="btn btn-primary"> ADD </button>
    </form>
</div>


<?php include_once 'footer.php';?>