<?php
/**
 * Created by PhpStorm.
 * User: DaiPhongPC
 * Date: 7/19/2018
 * Time: 5:00 PM
 */
class Connect{
    private $driver;
    private $host, $user, $pass, $database, $charset;
    private $connection;
    public function __construct()
    {
        $db_cfg = require_once 'config/database.php';
        $this->driver = db_driver;
        $this->host = db_host;
        $this->user = db_user;
        $this->pass = db_pass;
        $this->database = db_database;
        $this->charset = db_charset;
    }

    public function connection(){
        $data = $this->driver.':host='.$this->host.';dbname='.$this->database.';charset='.$this->charset;

        $this->connection = new mysqli($this->host, $this->user, $this->pass, $this->database);
        mysqli_query($this->connection, $this->charset);
        return $this->connection;
    }
}
