<?php
/**
 * Created by PhpStorm.
 * User: DaiPhongPC
 * Date: 7/19/2018
 * Time: 4:49 PM
 */
class ArticleController{
    private $connect;
    private $connection;

    public function __construct()
    {
        require_once __DIR__."/../core/Connect.php";
        require_once __DIR__."/../model/Article.php";
        require_once __DIR__."/../model/Member.php";
        require_once __DIR__."/../model/Prize.php";

        $this->connect = new Connect();
        $this->connection = $this->connect->connection();
    }

    public function run($action){
        switch ($action){
            case "index":
                $this->index();
                break;
            case "newArticle":
                $this->newArticle();
                break;
            case "newArticle2":
                $this->newArticle2();
                break;
            case "insertArticle":
                $this->insertArticle();
                break;
            case "insertUserArticle":
                $this->insertUserArticle();
                break;
            case "editArticle":
                $this->editArticle();
                break;
            case "editArticle2":
                $this->editArticle2();
                break;
            case "deleteArticle":
                $this->deleteArticle();
                break;
            case "deleteArticle2":
                $this->deleteArticle2();
                break;
            case "updateArticle":
                $this->updateArticle();
                break;
            case "updateArticle2":
                $this->updateArticle2();
                break;
            case "articleView":
                $this->articleView();
                break;
            case "article2View":
                $this->article2View();
                break;
            default:
                $this->index();
                break;
        }
    }

    public function articleView(){
        $article = new Article($this->connection);
        $articles = $article->getById_article($_GET["id_article"]);

        $this->view("article", array(
            "article" => $articles
        ));
    }

    public function article2View(){
        $article = new Article($this->connection);
        $articles = $article->getById_article($_GET["id_article"]);

        $this->view("article2", array(
            "article" => $articles
        ));
    }

    public function newArticle(){
        $this->view("addArticle", array(
            "title" => "Bài báo mới"
        ));
    }

    public function newArticle2(){
        $this->view("addArticle2", array(
            "title" => "Bài báo mới"
        ));
    }

    public function insertArticle(){
        if(isset($_POST["title"])){
            $article = new Article($this->connection);

            $article->setId_user($_POST["id_user"]);
            $article->setTitle($_POST["title"]);
            $article->setContent($_POST["content"]);
            $article->setCategory($_POST["category"]);
            $article->setCreate_date($_POST["create_date"]);

            $articleid = $article->insertArticle();

        }

        $prize = new Prize($this->connection);
        $prize->setId_article($articleid);
        $prize->setMedals($_POST["medals"]);

        $prize->insertPrize();

        header("Location: index.php?controller=member&action=index");
    }

    public function insertUserArticle(){
        if(isset($_POST["title"])){
            $article = new Article($this->connection);

            $article->setId_user($_POST["id_user"]);
            $article->setTitle($_POST["title"]);
            $article->setContent($_POST["content"]);
            $article->setCategory($_POST["category"]);
            $article->setCreate_date($_POST["create_date"]);

            $articleid = $article->insertArticle();

        }

        $prize = new Prize($this->connection);
        $prize->setId_article($articleid);
        $prize->setMedals($_POST["medals"]);

        $prize->insertPrize();

        header("Location: index.php?controller=member&action=user");
    }

    public function editArticle(){
        $article = new Article($this->connection);
        $articles = $article->getById_article($_GET["id_article"]);

        $this->view("editArticle", array(
            "article" => $articles
        ));
    }

    public function editArticle2(){
        $article = new Article($this->connection);
        $articles = $article->getById_article($_GET["id_article"]);

        $this->view("editArticle2", array(
            "article" => $articles
        ));
    }

    public function deleteArticle(){
        $article = new Article($this->connection);
        $articles = $article->deleteArticle($_GET["id_article"]);

        $prize = new Prize($this->connection);
        $prizes = $prize->deletePrize($_GET["id_article"]);

        header("Location: index.php?controller=member&action=index");
    }

    public function deleteArticle2(){
        $article = new Article($this->connection);
        $articles = $article->deleteArticle($_GET["id_article"]);

        $prize = new Prize($this->connection);
        $prizes = $prize->deletePrize($_GET["id_article"]);

        header("Location: index.php?controller=member&action=user");
    }

    public function updateArticle(){
        if(isset($_POST["id_article"])){
            $article = new Article($this->connection);

            $article->setId_article($_POST["id_article"]);
            $article->setId_user($_POST["id_user"]);
            $article->setTitle($_POST["title"]);
            $article->setContent($_POST["content"]);
            $article->setCategory($_POST["category"]);
            $article->setCreate_date($_POST["create_date"]);

            $articleid = $article->updateArticle();
        }

        $prize = new Prize($this->connection);
        $prize->setId_prize($_POST["id_prize"]);
        $prize->setId_article($articleid);
        $prize->setMedals($_POST["medals"]);
        $prize->editPrize($_POST["id_article"]);

        header("Location: index.php?controller=member&action=index");
    }

    public function updateArticle2(){
        if(isset($_POST["id_article"])){
            $article = new Article($this->connection);

            $article->setId_article($_POST["id_article"]);
            $article->setId_user($_POST["id_user"]);
            $article->setTitle($_POST["title"]);
            $article->setContent($_POST["content"]);
            $article->setCategory($_POST["category"]);
            $article->setCreate_date($_POST["create_date"]);

            $articleid = $article->updateArticle();
        }

        $prize = new Prize($this->connection);
        $prize->setId_prize($_POST["id_prize"]);
        $prize->setId_article($articleid);
        $prize->setMedals($_POST["medals"]);
        $prize->editPrize($_POST["id_article"]);

        header("Location: index.php?controller=member&action=user");
    }

    public function view($view, $data){
        $datas = $data;
        require_once __DIR__."/../view/".$view."View.php";
    }

}


?>