<?php
/**
 * Created by PhpStorm.
 * User: DaiPhongPC
 * Date: 7/19/2018
 * Time: 4:50 PM
 */
class PrizeController{
    private $connect;
    private $connection;

    public function __construct()
    {
        require_once __DIR__."/../core/Connect.php";
        require_once __DIR__."/../model/Prize.php";
        require_once __DIR__."/../model/Article.php";

        $this->connect = new Connect();
        $this->connection = $this->connect->connection();
    }

    public function run($action){
        switch ($action){
            case "index":
                $this->index();
                break;
            case "editPrize":
                $this->editPrize();
                break;
            case "updatePrize":
                $this->updatePrize();
                break;
            default:
                $this->index();
                break;
        }
    }

    public function editPrize(){
        $prize = new Prize($this->connection);
        $prizes = $prize->getById_prize($_GET["id_prize"]);

        $this->view("updatePrize", array(
            "prize" => $prizes
        ));
    }

    public function updatePrize(){
        if (isset($_POST["id_prize"])){
            $prize = new Prize($this->connection);

            $prize->setId_prize($_POST["id_prize"]);
            $prize->setMedals($_POST["medals"]);

            $prize->updatePrize();
        }
        header("Location: index.php?controller=member&action=index");
    }

    public function view($view, $data){
        $datas = $data;
        require_once __DIR__."/../view/". $view."View.php";
    }
}


?>