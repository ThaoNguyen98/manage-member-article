<?php
/**
 * Created by PhpStorm.
 * User: DaiPhongPC
 * Date: 7/19/2018
 * Time: 4:49 PM
 */
session_start();

class MemberController{
    private $connect;
    private $connection;

    public function __construct()
    {
        require_once __DIR__."/../core/Connect.php";
        require_once __DIR__."/../model/Member.php";
        require_once __DIR__."/../model/Article.php";
        require_once __DIR__."/../model/Prize.php";

        $this->connect = new Connect();
        $this->connection = $this->connect->connection();
    }

    public function run($action){
        switch($action)
        {
            case "index" :
                $this->index();
                break;
            case "login" :
                $this->loginAction();
                break;
            case "nomal" :
                $this->nomalView();
                break;
            case "user" :
                $this->user();
                break;
            case "newMember" :
                $this->newMember();
                break;
            case "register" :
                $this->register();
                break;
            case "editMember" :
                $this->editMember();
                break;
            case "insertMember" :
                $this->insertMember();
                break;
            case "registerAc" :
                $this->registerAc();
                break;
            case "deleteMember" :
                $this->deleteMember();
                break;
            case "updateMember" :
                $this->updateMember();
                break;
            case "updateUser" :
                $this->updateUser();
                break;
            default:
                $this->index();
                break;
        }
    }

    public function index(){

        $member = new Member($this->connection);
        $members = $member->getAll();

        $article = new Article($this->connection);
        $articles = $article->getAll();

        $prize = new Prize($this->connection);
        $prizes = $prize->getAll();

        $this->view("index", array(
            "member" => $members,
            "article" => $articles,
            "prize" => $prizes
        ));
    }

    public function nomalView(){

        $member = new Member($this->connection);
        $members = $member->getAll();

        $article = new Article($this->connection);
        $articles = $article->getAll();

        $prize = new Prize($this->connection);
        $prizes = $prize->getAll();

        $this->view("nomal", array(
            "member" => $members,
            "article" => $articles,
            "prize" => $prizes
        ));
    }

    public function user(){
        $id = $_SESSION['id'];

        $member = new Member($this->connection);
        $members = $member->getById_user($id);

        $article = new Article($this->connection);
        $articles = $article->getById_user($id);

        $prize = new Prize($this->connection);
        $prizes = $prize->getAll();

        $this->view("user", array(
            "member" => $members,
            "article" => $articles,
            "prize" => $prizes
        ));
    }

    public function loginAction(){
        $message = "";
        if(isset($_POST['submit'])){

            $member = new Member($this->connection);

            $userLogin = $_POST["username"];
            $passLogin = $_POST["password"];

            $a = $member->checkuser($userLogin, $passLogin);

            if(mysqli_num_rows($a) == 1) {
                    $b = mysqli_fetch_array($a);

                    $_SESSION['USERNAME'] = $b["USERNAME"];
                    $_SESSION['ROLE'] = $b["ROLE"];
                    $_SESSION['id'] = $b["ID_USER"];

                if($_SESSION['ROLE'] == 1){

                    $_SESSION['username']=$_POST["username"];
                    header("Location: index.php?controller=member&action=index");
                }
                else if($_SESSION['ROLE'] == 0){

                    $_SESSION['username']=$_POST["username"];

                    header("Location: index.php?controller=member&action=nomal");
                }
            }
            else {
                $message = "Tên đăng nhập hoặc mật khẩu không đúng";
            }
        }
        $this->view("login", array(
            "message" => $message
        ));
    }

    public function newMember(){
        $this->view("addMember", array(
            "title" => "Hội viên mới"
        ));
    }

    public function register(){
        $this->view("register", array(
            "title" => "Đăng ký"
        ));
    }

    public function editMember(){
        $member = new Member($this->connection);
        $members = $member->getById_user($_GET["id_user"]);

        $this->view("editMember", array(
            "member" => $members
        ));
    }

    public function insertMember(){
        if(isset($_POST["first_name"])){

            $member = new Member($this->connection);
            $member->setFirstname($_POST["first_name"]);
            $member->setSurname($_POST["surname"]);
            $member->setUsername($_POST["username"]);
            $member->setPassword($_POST["password"]);
            $member->setJoindate($_POST["join_date"]);
            $member->setHometown($_POST["hometown"]);
            $member->setRole($_POST["role"]);

            $save = $member->insertMember();
        }
        $this->run("index");
    }

    public function registerAc(){
        if(isset($_POST["first_name"])){

            $member = new Member($this->connection);
            $member->setFirstname($_POST["first_name"]);
            $member->setSurname($_POST["surname"]);
            $member->setUsername($_POST["username"]);
            $member->setPassword($_POST["password"]);
            $member->setJoindate($_POST["join_date"]);
            $member->setHometown($_POST["hometown"]);
            $member->setRole($_POST["role"]);

            $save = $member->insertMember();
        }
        $this->run("nomal");
    }

    public function deleteMember(){
        $member = new Member($this->connection);
        $member = $member->deleteMember($_GET["id_user"]);

        $this->run("index");
    }

    public function updateUser(){
        if(isset($_POST["submit"])){

            $member = new Member($this->connection);

            $member->setId_user($_SESSION['id']);
            $member->setFirstname($_POST["first_name"]);
            $member->setSurname($_POST["surname"]);
            $member->setUsername($_POST["username"]);
            $member->setPassword($_POST["password"]);
            $member->setJoindate($_POST["join_date"]);
            $member->setHometown($_POST["hometown"]);
            $member->setRole($_POST["role"]);

            $member->updateMember();

        }
        header("Location: index.php?controller=member&action=nomal");
    }

    public function updateMember(){
        if(isset($_POST["id_user"])){

            $member = new Member($this->connection);

            $member->setId_user($_POST["id_user"]);
            $member->setFirstname($_POST["first_name"]);
            $member->setSurname($_POST["surname"]);
            $member->setUsername($_POST["username"]);
            $member->setPassword($_POST["password"]);
            $member->setJoindate($_POST["join_date"]);
            $member->setHometown($_POST["hometown"]);
            $member->setRole($_POST["role"]);

            $member->updateMember();

        }
        header("Location: index.php?controller=member&action=index");
    }

    public function view($view,$data){
        extract($data);
        ob_start();
        require_once  __DIR__ . "/../view/" . $view . "View.php";
    }


}
