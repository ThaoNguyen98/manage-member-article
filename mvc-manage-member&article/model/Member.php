<?php
/**
 * Created by PhpStorm.
 * User: DaiPhongPC
 * Date: 7/19/2018
 * Time: 5:21 PM
 */

class Member{
    protected $table = "";
    protected $connection;
    protected $id_user;

    private $first_name;
    private $surname;
    private $username;
    private $password;
    private $join_date;
    private $hometown;
    private $role;

    public function __construct($connection)
    {
        require_once __DIR__."/../config/global.php";
        $this->connection = $connection;
        $this->table = table_member;
    }

    public function getId_user(){
        return $this->id_user;
    }

    public function setId_user($id_user){
        $this->id_user = $id_user;
    }

    public function getFirstname(){
        return $this->first_name;
    }

    public function setFirstname($first_name){
        $this->first_name = $first_name;
    }

    public function getSurname(){
        return $this->surname;
    }

    public function setSurname($surname){
        $this->surname = $surname;
    }

    public function getUsername(){
        return $this->username;
    }

    public function setUsername($username){
        $this->username = $username;
    }

    public function getPassword(){
        return $this->username;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    public function getJoindate(){
        return $this->join_date;
    }

    public function setJoindate($join_date){
        $this->join_date = $join_date;
    }

    public function getHometown(){
        return $this->hometown;
    }

    public function setHometown($hometown){
        $this->hometown = $hometown;
    }

    public function getRole(){
        return $this->role;
    }

    public function setRole($role){
        $this->role = $role;
    }

    public function getAll(){
        $sql = "SELECT * FROM ".$this->table;

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function getById_user($id_user){
        $sql = "SELECT * FROM ".$this->table." WHERE ID_USER = $id_user";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function insertMember(){
        $sql = "INSERT INTO ".$this->table." (`FIRST_NAME`, `SURNAME`, `USERNAME`, `PASSWORD`, `JOIN_DATE`, `HOMETOWN`, `ROLE`) 
                VALUES ('$this->first_name', '$this->surname', '$this->username','$this->password', '$this->join_date', '$this->hometown', '$this->role')";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function deleteMember($id_user){
        $sql = "DELETE FROM ".$this->table." WHERE ID_USER = $id_user";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }
    public function updateMember(){
        $sql = "UPDATE `".$this->table."`
                SET 
                    `FIRST_NAME` = '$this->first_name',
                    `SURNAME` = '$this->surname',
                    `USERNAME` = '$this->username',  
                    `PASSWORD` = '$this->password',              
                    `JOIN_DATE` = '$this->join_date',
                    `HOMETOWN` = '$this->hometown',
                    `ROLE` = '$this->role'
                    
                WHERE `ID_USER` = $this->id_user
                ";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function checkuser($username, $password){
        $sql = "SELECT * FROM ".$this->table." WHERE USERNAME='$username' AND PASSWORD='$password'";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function setSession($key, $val){
        $_SESSION[$key] = $val;
    }



}

?>
