<?php
/**
 * Created by PhpStorm.
 * User: DaiPhongPC
 * Date: 7/24/2018
 * Time: 2:21 PM
 */

class Article{
    private $table = "";
    private $tablePrize = "";
    private $connection;

    private $id_article;
    private $id_user;
    private $title;
    private $content;
    private $category;
    private $create_date;
    private $prize;

    public function __construct($connection)
    {
        require_once __DIR__."/../config/global.php";
        $this->connection = $connection;
        $this->table = table_article;
        $this->tablePrize = table_prize;
    }

    public function getId_article(){
        return $this->id_article;
    }

    public function setId_article($id_article){
        $this->id_article = $id_article;
    }

    public function getId_user(){
        return $this->id_user;
    }

    public function setId_user($id_user){
        $this->id_user = $id_user;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function getContent(){
        return $this->content;
    }

    public function setContent($content){
        $this->content = $content;
    }

    public function getCategory(){
        return $this->category;
    }

    public function setCategory($category){
        $this->category = $category;
    }

    public function getCreate_date(){
        return $this->create_date;
    }

    public function setCreate_date($create_date){
        $this->create_date = $create_date;
    }

    public function getPrize(){
        return $this->prize;
    }

    public function setPrize($prize){
        $this->prize = $prize;
    }

    public function getAll(){
        $sql = "SELECT * FROM ".$this->table;

        $query = $this->connection->query($sql);
        $this->connection = null;

        return $query;
    }

    public function getById_article($id_article){
        $sql = "SELECT * FROM ".$this->table." WHERE ID_ARTICLE = $id_article";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function getById_user($id_user){
        $sql = "SELECT * FROM ".$this->table." WHERE ID_USER = $id_user";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function insertArticle(){
        $sql = "INSERT INTO ".$this->table."(`ID_USER`, `TITLE`, `CONTENT`, `CATEGORY`, `CREATE_DATE`)
                VALUE ('$this->id_user', '$this->title', '$this->content', '$this->category', '$this->create_date')";

        if(mysqli_query($this->connection, $sql)){
            $id = mysqli_insert_id($this->connection);
            return $id;
        }
//        $query = $this->connection->query($sql);
//
//        $this->connection = null;
//
//        return $query;
    }

    public function deleteArticle($id_article){
        $sql = "DELETE FROM ".$this->table." WHERE ID_ARTICLE = $id_article";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function getDataPrize(){
        $sql = "SELECT ".$this->tablePrize.".MEDALS FROM ".$this->table.", ".$this->tablePrize." WHERE ".$this->table.".ID_ARTICLE = ".$this->tablePrize.".ID_ARTICLE";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function updateArticle(){
        $sql = "UPDATE ".$this->table." 
                SET 
                    `ID_USER` = '$this->id_user',
                    `TITLE` = '$this->title',
                    `CONTENT` = '$this->content',
                    `CATEGORY` = '$this->category',
                    `CREATE_DATE` = '$this->create_date'
                    
                WHERE `ID_ARTICLE` = $this->id_article
                ";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

}



?>