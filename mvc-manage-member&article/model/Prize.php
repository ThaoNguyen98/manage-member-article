<?php
/**
 * Created by PhpStorm.
 * User: DaiPhongPC
 * Date: 7/27/2018
 * Time: 10:03 AM
 */
class Prize{
    protected $table = "";
    protected $connection;
    protected $id_prize;

    private $id_article;
    private $medals;

    public function __construct($connection)
    {
        require_once __DIR__."/../config/global.php";
        $this->connection = $connection;
        $this->table = table_prize;
    }

    public function getId_prize(){
        return $this->id_prize;
    }

    public function setId_prize($id_prize){
        $this->id_prize = $id_prize;
    }

    public function getId_article(){
        return $this->id_article;
    }

    public function setId_article($id_article){
        $this->id_article = $id_article;
    }

    public function getMedals(){
        return $this->medals;
    }

    public function setMedals($medals){
        $this->medals = $medals;
    }

    public function getAll(){
        $sql = "SELECT * FROM ".$this->table;
        $query = $this->connection->query($sql);
        $this->connection = null;

        return $query;
    }

    public function getById_prize($id_prize){
        $sql = "SELECT * FROM ".$this->table." WHERE ID_PRIZE = $id_prize";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function insertPrize(){
        $sql = "INSERT INTO ".$this->table." (`ID_ARTICLE`, `MEDALS`) VALUES ('$this->id_article', '$this->medals')";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function deletePrize($id_article){
        $sql = "DELETE FROM ".$this->table." WHERE ID_ARTICLE = $id_article";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function editPrize($id_article){
        $sql = "UPDATE `".$this->table."` 
                SET 
                    `MEDALS` = '$this->medals'
                WHERE `ID_ARTICLE` = $id_article
                ";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }

    public function updatePrize(){
        $sql = "UPDATE `".$this->table."` 
                SET 
                    `MEDALS` = '$this->medals'
                WHERE `ID_PRIZE` = $this->id_prize
                ";

        $query = $this->connection->query($sql);

        $this->connection = null;

        return $query;
    }
}


?>