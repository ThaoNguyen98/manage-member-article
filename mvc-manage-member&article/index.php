<?php
require_once 'config/global.php';

if(isset($_GET["controller"])){
    $controllerObj = loadController($_GET["controller"]);

    launchAction($controllerObj);
}else{
    $controllerObj = loadController(controller_defective);

    launchAction($controllerObj);
}

function loadController($controller){
    $control = ucwords($controller).'Controller';

    $strFileController = 'controller/'.$control.'.php';

    if(!is_file($strFileController)){
        $strFileController = 'controller/'.ucwords(controller_defective);
    }

    require_once $strFileController;

    $controllerObj = new $control;
    return $controllerObj;
}

function launchAction($controlleObj){
    if(isset($_GET["action"])){
        $controlleObj->run($_GET["action"]);
    } else{
        $controlleObj->run(action_defective);
    }
}

?>

